//
//  ResultViewController.m
//  Rock-Paper-Scissor
//
//  Created by Marcelo Macri on 7/28/14.
//  Copyright (c) 2014 marcelomacri. All rights reserved.
//

#import "ResultViewController.h"

@interface ResultViewController ()

@end

@implementation ResultViewController

NSInteger winner;

- (void) setupMySelection{
    [_myNameLabel setText:_myName];
    switch (_mySelectionNumber) {
        case 0:
            [_mySelectionImage setImage:_zeroFingerImage];
            break;
        case 1:
            [_mySelectionImage setImage:_oneFingerImage];
            break;
        case 2:
            [_mySelectionImage setImage:_twoFingerImage];
            break;
        case 3:
            [_mySelectionImage setImage:_threeFingerImage];
            break;
        case 4:
            [_mySelectionImage setImage:_fourFingerImage];
            break;
        case 5:
            [_mySelectionImage setImage:_fiveFingerImage];
            break;
        default:
            break;
    }
}

- (void) setupOpponentSelection{
    [_opponentNameLabel setText:_opponentName];
    switch (_opponentSelectionNumber) {
        case 0:
            [_opponentSelectionImage setImage:_zeroFingerImage];
            break;
        case 1:
            [_opponentSelectionImage setImage:_oneFingerImage];
            break;
        case 2:
            [_opponentSelectionImage setImage:_twoFingerImage];
            break;
        case 3:
            [_opponentSelectionImage setImage:_threeFingerImage];
            break;
        case 4:
            [_opponentSelectionImage setImage:_fourFingerImage];
            break;
        case 5:
            [_opponentSelectionImage setImage:_fiveFingerImage];
            break;
        default:
            break;
    }
}

- (void) determineWinner{
    //odd/even selection number:
    //0 is odd
    //1 is even
    
    //winner:
    //1 - i win
    //2 - i lose
    
    NSInteger temp = _mySelectionNumber + _opponentSelectionNumber;
    
    switch (_myOddEvenSelectionNumber) {
        case 0:
            //result has to be odd
            if (temp % 2 == 0) {
                NSLog(@"you lose");
                winner = 2;
            }
            else{
                NSLog(@"you win");
                winner = 1;
            }
            break;
        case 1:
            //result has to be even
            if (temp % 2 == 0) {
                NSLog(@"you win");
                winner = 1;
            }
            else{
                NSLog(@"you lose");
                winner = 2;
            }
        default:
            break;
    }
}

- (void) displayWinnerAlert {
    // 1 - i win
    // 2 - i lose
    
    UIAlertView *alert;
    
    switch (winner) {
        case 1:
            alert = [[UIAlertView alloc]
                                  initWithTitle:@""
                                  message:@"YOU WIN !!!"
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles: nil];
            [alert show];
            break;
        case 2:
            alert = [[UIAlertView alloc]
                                  initWithTitle:@""
                                  message:@"YOU LOSE !!!"
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles: nil];
            [alert show];
            break;
        default:
            break;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _zeroFingerImage = [UIImage imageNamed:@"zero.png"];
    _oneFingerImage = [UIImage imageNamed:@"one.png"];
    _twoFingerImage = [UIImage imageNamed:@"two.png"];
    _threeFingerImage = [UIImage imageNamed:@"three.png"];
    _fourFingerImage = [UIImage imageNamed:@"four.png"];
    _fiveFingerImage = [UIImage imageNamed:@"five.png"];
    
    NSLog(@"my name: %@", _myName);
    NSLog(@"my odd/even selection number: %d", _myOddEvenSelectionNumber);
    NSLog(@"my selection number: %d", _mySelectionNumber);
    NSLog(@"opponent name: %@", _opponentName);
    NSLog(@"opponent odd/even selection number: %d", _opponentOddEvenSelectionNumber);
    NSLog(@"opponent selection number: %d", _opponentSelectionNumber);
    
    [self setupMySelection];
    [self setupOpponentSelection];
    [self determineWinner];
    [self displayWinnerAlert];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
