//
//  ViewController.m
//  MCDemo
//
//  Created by Marcelo Macri on 6/6/14.
//  Copyright (c) 2014 marcelomacri. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "ResultViewController.h"

@interface ViewController ()

@property (nonatomic, strong) AppDelegate *appDelegate;

-(void)sendMyMessage;
-(void)didReceiveDataWithNotification:(NSNotification *)notification;

@end

@implementation ViewController

NSString *myName;
NSString *opponentName;
NSInteger mySelectionNumber;
NSInteger myOddEvenSelectionNumber;
NSInteger opponentSelectionNumber;
NSInteger opponentOddEvenSelectionNumber;
NSInteger sentSuccess;
NSInteger receivedSuccess;

- (IBAction) goButtonPressed {
    NSLog(@"go Button Pressed");
    NSLog(@"Number of session connected peers: %d", _appDelegate.mcManager.session.connectedPeers.count);
    NSLog(@"size of connected peer array %d", _appDelegate.mcManager.connectedPeers.count);
    NSLog(@"name of connected peer %@", _appDelegate.mcManager.connectedPeers);
    
    if (_appDelegate.mcManager.session.connectedPeers.count > 0) {
        [_goButton setTitle:@"LOADING..." forState:UIControlStateNormal];
        [_goButton setEnabled:false];
        [_oddEvenSegmentControl setEnabled:false];
        [_selectionStepper setEnabled:false];
        [self sendMyMessage];
        [self pushResultViewController];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@""
                              message:@"Make sure you are connected to another player before clicking go.\n"
                              delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles: nil];
        [alert show];
    }
}

- (void) pushResultViewController{
    NSLog(@"Push result view controller");
    if (sentSuccess == 1 && receivedSuccess == 1) {
        ResultViewController *rvc = [self.storyboard instantiateViewControllerWithIdentifier:@"ResultVC"];
        rvc.myName = myName;
        rvc.myOddEvenSelectionNumber = myOddEvenSelectionNumber;
        rvc.mySelectionNumber = mySelectionNumber;
        rvc.opponentName = opponentName;
        rvc.opponentOddEvenSelectionNumber = opponentOddEvenSelectionNumber;
        rvc.opponentSelectionNumber = opponentSelectionNumber;
        
        if (myOddEvenSelectionNumber == 0 && opponentOddEvenSelectionNumber == 0) {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Alert"
                                  message:@"Both cannot be odd. Try again."
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles: nil];
            [alert show];
            [_goButton setEnabled:true];
            [_oddEvenSegmentControl setEnabled:true];
            [_selectionStepper setEnabled:true];
            [_goButton setTitle:@"GO !!!" forState:UIControlStateNormal];
            sentSuccess = 0;
            receivedSuccess = 0;
        }
        else if (myOddEvenSelectionNumber == 1 && opponentOddEvenSelectionNumber == 1) {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Alert"
                                  message:@"Both cannot be even. Try again."
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles: nil];
            [alert show];
            [_goButton setEnabled:true];
            [_oddEvenSegmentControl setEnabled:true];
            [_selectionStepper setEnabled:true];
            [_goButton setTitle:@"GO !!!" forState:UIControlStateNormal];
            sentSuccess = 0;
            receivedSuccess = 0;
        }
        else{
            [_goButton setEnabled:true];
            [_oddEvenSegmentControl setEnabled:true];
            [_selectionStepper setEnabled:true];
            [_goButton setTitle:@"GO !!!" forState:UIControlStateNormal];
            sentSuccess = 0;
            receivedSuccess = 0;
            [self.navigationController pushViewController:rvc animated:YES];
        }
    } else {
        NSLog(@"sentSuccess: %d receivedSuccess: %d", sentSuccess, receivedSuccess);
        [NSTimer scheduledTimerWithTimeInterval:2.0
                                         target:self
                                       selector:@selector(pushResultViewController)
                                       userInfo:nil
                                        repeats:NO];
    }
}

- (IBAction) selectionStepperPressed {
    NSLog(@"selection stepper pressed");
    mySelectionNumber = _selectionStepper.value;
    NSLog(@"my selection stepper value: %d", mySelectionNumber);
    switch (mySelectionNumber) {
        case 0:
            [_selectionImage setImage:_zeroFingerImage];
            break;
        case 1:
            [_selectionImage setImage:_oneFingerImage];
            break;
        case 2:
            [_selectionImage setImage:_twoFingerImage];
            break;
        case 3:
            [_selectionImage setImage:_threeFingerImage];
            break;
        case 4:
            [_selectionImage setImage:_fourFingerImage];
            break;
        case 5:
            [_selectionImage setImage:_fiveFingerImage];
            break;
        default:
            break;
    }
}

- (IBAction) oddEvenSegmentControlPressed{
    NSLog(@"ood even segment control pressed");
    myOddEvenSelectionNumber = _oddEvenSegmentControl.selectedSegmentIndex;
    NSLog(@"segment control index: %d", myOddEvenSelectionNumber);
}

-(void)sendMyMessage{
    NSLog(@"send my message");
    NSString *oddEvenNum = [NSString stringWithFormat:@"%d", myOddEvenSelectionNumber];
    NSString *selectionNum = [NSString stringWithFormat:@"%d", mySelectionNumber];
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    [dictionary setObject:oddEvenNum forKey:@"oddEvenNum"];
    [dictionary setObject:selectionNum forKey:@"selectionNum"];
    NSData *dataToSend = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
    NSArray *allPeers = _appDelegate.mcManager.session.connectedPeers;
    NSError *error;
    
    [_appDelegate.mcManager.session sendData:dataToSend
                                     toPeers:allPeers
                                    withMode:MCSessionSendDataReliable
                                       error:&error];
    
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:@"There was a connection error.  Please try again."
                              delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles: nil];
        [alert show];
        [_goButton setEnabled:true];
        [_oddEvenSegmentControl setEnabled:true];
        [_selectionStepper setEnabled:true];
    }else {
        sentSuccess = 1;
    }
}

-(void)didReceiveDataWithNotification:(NSNotification *)notification{
    MCPeerID *peerID = [[notification userInfo] objectForKey:@"peerID"];
    NSString *peerDisplayName = peerID.displayName;
    NSData *receivedData = [[notification userInfo] objectForKey:@"data"];
    NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:receivedData];
    NSString *oddEvenNum = [dictionary objectForKey:@"oddEvenNum"];
    NSString *selectionNum = [dictionary objectForKey:@"selectionNum"];
    NSInteger temp1 = [oddEvenNum integerValue];
    NSInteger temp2 = [selectionNum integerValue];
    opponentName = peerDisplayName;
    opponentOddEvenSelectionNumber = temp1;
    opponentSelectionNumber = temp2;
    NSLog(@"%@, %d, %d", opponentName, opponentSelectionNumber, opponentOddEvenSelectionNumber);
    receivedSuccess = 1;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _zeroFingerImage = [UIImage imageNamed:@"zero.png"];
    _oneFingerImage = [UIImage imageNamed:@"one.png"];
    _twoFingerImage = [UIImage imageNamed:@"two.png"];
    _threeFingerImage = [UIImage imageNamed:@"three.png"];
    _fourFingerImage = [UIImage imageNamed:@"four.png"];
    _fiveFingerImage = [UIImage imageNamed:@"five.png"];
    
    NSString *name = [[UIDevice currentDevice] name];
    myName = name;
    
    myOddEvenSelectionNumber = _oddEvenSegmentControl.selectedSegmentIndex;
    mySelectionNumber = _selectionStepper.value;
    
    sentSuccess = 0;
    receivedSuccess = 0;
    
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (_appDelegate.mcManager.session.connectedPeers.count < 1) {
        
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Alert"
                              message:@"Connect with another player, make selection, and click go!!!"
                              delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles: nil];
        [alert show];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didReceiveDataWithNotification:)
                                                     name:@"MCDidReceiveDataNotification"
                                                   object:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

