//
//  ViewController.h
//  MCDemo
//
//  Created by Marcelo Macri on 6/6/14.
//  Copyright (c) 2014 marcelomacri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultViewController.h"

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIButton *goButton;
@property (nonatomic, strong) IBOutlet UIImageView *selectionImage;
@property (nonatomic, strong) IBOutlet UIStepper *selectionStepper;
@property (nonatomic, strong) IBOutlet UISegmentedControl *oddEvenSegmentControl;

@property (nonatomic, strong) UIImage *zeroFingerImage;
@property (nonatomic, strong) UIImage *oneFingerImage;
@property (nonatomic, strong) UIImage *twoFingerImage;
@property (nonatomic, strong) UIImage *threeFingerImage;
@property (nonatomic, strong) UIImage *fourFingerImage;
@property (nonatomic, strong) UIImage *fiveFingerImage;

- (IBAction) goButtonPressed;
- (IBAction) selectionStepperPressed;
- (IBAction) oddEvenSegmentControlPressed;

@end
