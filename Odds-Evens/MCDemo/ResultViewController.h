//
//  ResultViewController.h
//  Rock-Paper-Scissor
//
//  Created by Marcelo Macri on 7/28/14.
//  Copyright (c) 2014 marcelomacri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewController : UIViewController

@property NSString *myName;
@property NSInteger mySelectionNumber;
@property NSInteger myOddEvenSelectionNumber;

@property NSString *opponentName;
@property NSInteger opponentSelectionNumber;
@property NSInteger opponentOddEvenSelectionNumber;

@property (nonatomic, strong) UIImage *zeroFingerImage;
@property (nonatomic, strong) UIImage *oneFingerImage;
@property (nonatomic, strong) UIImage *twoFingerImage;
@property (nonatomic, strong) UIImage *threeFingerImage;
@property (nonatomic, strong) UIImage *fourFingerImage;
@property (nonatomic, strong) UIImage *fiveFingerImage;

@property (nonatomic, strong) IBOutlet UIImageView *mySelectionImage;
@property (nonatomic, strong) IBOutlet UIImageView *opponentSelectionImage;
@property (nonatomic, strong) IBOutlet UILabel *myNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *opponentNameLabel;


@end
