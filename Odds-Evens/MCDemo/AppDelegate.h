//
//  AppDelegate.h
//  MCDemo
//
//  Created by Marcelo Macri on 6/6/14.
//  Copyright (c) 2014 marcelomacri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCManager.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) MCManager *mcManager;


@end
